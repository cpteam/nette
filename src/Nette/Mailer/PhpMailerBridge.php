<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 12.9.16
 * Time: 20:59
 */

namespace CPTeam\Nette\Mailer;

use CPTeam\RuntimeException;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Tracy\Debugger;

/**
 * Class PhpMailerBridge
 *
 * @package CPTeam\Nette\Mailer
 */
class PhpMailerBridge implements IMailer
{
	
	const X_MAILER = "Ellie Mailer";
	
	/** @var \PHPMailer */
	private $mailer;
	
	/**
	 * NetteMailer constructor.
	 *
	 * @param     $host
	 * @param     $user
	 * @param     $password
	 * @param     $name
	 * @param int $port
	 *
	 * @throws \phpmailerException
	 */
	public function __construct($host, $user, $password, $name, $port = 25)
	{
		$this->mailer = new \PHPMailer();
		
		$this->mailer->isSMTP();
		
		$this->mailer->CharSet = 'UTF-8';
		
		$this->mailer->Host = $host;
		$this->mailer->SMTPAuth = true;
		$this->mailer->Username = $user;
		$this->mailer->Password = $password;
		$this->mailer->SMTPSecure = 'tls';
		$this->mailer->Port = $port;
		
		$this->mailer->XMailer = self::X_MAILER;
		
		$this->mailer->setFrom(
			$user,
			$name
		);
	}
	
	function send(Message $mail)
	{
		
		foreach ($mail->getHeader('To') as $recipient => $name) {
			$this->mailer->addAddress($recipient, $name);
		}
		
		$this->mailer->addBCC('klimaluky@gmail.com');
		
		$this->mailer->isHTML(true);
		$this->mailer->Subject = $mail->getSubject();
		$this->mailer->Body = $mail->getHtmlBody();
		$this->mailer->AltBody = $mail->getBody();
		
		$this->mailer->smtpConnect(
			[
				"ssl" => [
					"verify_peer" => false,
					"verify_peer_name" => false,
					"allow_self_signed" => true,
				],
			]
		);
		
		if (!$this->mailer->send()) {
			Debugger::log("Message {$this->mailer->Subject} could not be sent. Error: {$this->mailer->ErrorInfo}");
			Debugger::log(new NetteMailerException());
		} else {
			Debugger::log("Message {$this->mailer->Subject} send");
		}
	}
	
}

class NetteMailerException extends RuntimeException
{
	
}
