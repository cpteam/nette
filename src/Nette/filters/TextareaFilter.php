<?php
/**
 * Created by PhpStorm.
 * User: lukask
 * Date: 25.11.15
 * Time: 10:28
 */

namespace CPTeam\Nette\Filters;

use CPTeam\Utils\StringEscape;
use Nette;
use Nette\Utils\Html;

class TextareaFilter extends Nette\Object
{
	
	public static function get($text)
	{
		
		$text = StringEscape::escapeHtml($text);
		
		$text = StringEscape::markDown($text);
		
		$text = StringEscape::char($text);
		$text = StringEscape::trim($text);
		
		$text = StringEscape::url($text);
		$text = StringEscape::createList($text);
		
		$text = str_ireplace("\n", "<br />", $text);
		
		return Html::el("div", [
			'class' => 'enchancedText',
		])->addHtml(
			$text
		);
	}
	
}
