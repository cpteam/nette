<?php
/**
 * Created by PhpStorm.
 * User: lukask
 * Date: 25.11.15
 * Time: 10:28
 */

namespace CPTeam\Nette\Filters;

use Nette;

/**
 * Class AgeFilter
 *
 * @package CPTeam\Nette\Filters
 */
class AgeFilter extends Nette\Object
{
	
	public function __invoke($dob)
	{
		
		if ($dob > 10000000) {
			$dob = "" . $dob . "";
			$day = $dob[6] . $dob[7];
			$month = $dob[4] . $dob[5];
			$year = $dob[0] . $dob[1] . $dob[2] . $dob[3];
			$now = [date('d'), date('m'), date('Y')];
			$ccaAge = $now[2] - $year - 1;
			if ($month > $now[1]) $age = $ccaAge;
			elseif ($month == $now[1]) {
				if ($day > $now[0]) $age = $ccaAge;
				else $age = $ccaAge + 1;
			} else $age = $ccaAge + 1;
			
			return $age;
		} else return "N/A";
	}
	
}
