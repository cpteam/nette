<?php
/**
 * Created by PhpStorm.
 * User: lukask
 * Date: 25.11.15
 * Time: 10:28
 */

namespace CPTeam\Nette\Filters;

use Nette;

class DateFilter extends Nette\Object
{
	
	public function __invoke($time, $format = null)
	{
		if ($format == null) {
			$format = "d. m. Y H:i";
		} elseif ($format == "hi") {
			$format = "d. m. Y H:i";
		} elseif ($format == "his") {
			$format = "d. m. Y H:i:s";
		}
		
		return date($format, $time);
	}
	
}
