<?php
/**
 * Created by PhpStorm.
 * User: lukask
 * Date: 25.11.15
 * Time: 10:28
 */

namespace CPTeam\Nette\Filters;

use Nette;

class IsVerifiedIconFilter extends Nette\Object
{
	
	public function __invoke($var)
	{
		if ($var == 1) {
			$html = new Nette\Utils\Html();
			
			$c = $html::el()->setHtml(
				'<span title="Ověřená stránka"><i class="fa fa-check-circle"></i></span>'
			);
			
			return $c;
		}
		
		return '';
	}
	
}
