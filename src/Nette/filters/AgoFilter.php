<?php
/**
 * Created by PhpStorm.
 * User: lukask
 * Date: 25.11.15
 * Time: 10:28
 */

namespace CPTeam\Nette\Filters;

use Nette;

class AgoFilter extends Nette\Object
{
	
	public static function get($date)
	{
		if ($date instanceof \DateTime) {
			$date = $date->getTimestamp();
		}
		
		$res = time() - $date;
		if ($res < 3600) {
			if ($res < 60) return 'Teď';
			else return (floor($res / 60)) . ' min';
		} elseif ($res < 86400) {
			/*if($res < 3600*2) return '1 hod';
			else*/
			return (floor($res / 3600)) . ' hod';
		}
		
		return date('d.m.Y H:i', $date);
	}
	
}
