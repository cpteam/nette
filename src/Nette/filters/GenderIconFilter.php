<?php
/**
 * Created by PhpStorm.
 * User: lukask
 * Date: 25.11.15
 * Time: 10:28
 */

namespace CPTeam\Nette\Filters;

use Nette;

class GenderIconFilter extends Nette\Object
{
	
	public static function get($gender, $type = 'full')
	{
		if ($gender === null) $gender = 0;
		$g = [
			'<span class="colorGrey"><i class="fa fa-question "></i> Neuvedeno</span>',
			'<span class="colorMale"><i class="fa fa-male"></i> Muž</span>',
			'<span class="colorFemale"><i class="fa fa-female"></i> Žena</span>',
		];
		
		return Nette\Utils\Html::el()->setHtml($g[$gender]);
	}
	
}
