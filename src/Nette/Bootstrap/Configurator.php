<?php

namespace CPTeam\Nette\Bootstrap;

use Nette;

class Configurator extends \Nette\Configurator
{
	public function createRobotLoader($forceBuild = false)
	{
		if (!class_exists(Nette\Loaders\RobotLoader::class)) {
			throw new Nette\NotSupportedException('RobotLoader not found, do you have `nette/robot-loader` package installed?');
		}
		
		$loader = new Nette\Loaders\RobotLoader;
		$loader->setCacheStorage(new Nette\Caching\Storages\FileStorage($this->getCacheDirectory()));
		$loader->autoRebuild = $this->parameters['debugMode'] || $forceBuild;
		
		return $loader;
	}
	
}
