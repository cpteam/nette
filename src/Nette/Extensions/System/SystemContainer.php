<?php

namespace CPTeam\Nette\Extensions\System;

/**
 * Class SystemContainerService
 *
 * @package CPTeam\Nette\Extensions\System
 * @author CP Team <dev@cosplan.cz>
 * @license MIT
 * @link <http://dev.cosplan.cz>
 */
class SystemContainer
{
	/**
	 * @var string
	 */
	private $privateKey = null;
	
	/**
	 * @return string
	 */
	public function getPrivateKey()
	{
		return $this->privateKey;
	}
	
	/**
	 * @param string $privateKey
	 *
	 * @internal
	 */
	public function setPrivateKey($privateKey)
	{
		$this->privateKey = $privateKey;
	}
	
}
