<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 2.10.16
 * Time: 22:30
 */

namespace CPTeam\Nette\Extensions\System;

use Nette\DI\CompilerExtension;
use Nette\Utils\Random;

class SystemExtension extends CompilerExtension
{
	private $defaults = [];
	
	public function loadConfiguration()
	{
		$config = $this->getConfig($this->defaults);
		
		$builder = $this->getContainerBuilder();
		
		$container = $builder->addDefinition($this->prefix("container"))
			->setClass(SystemContainer::class)
			->addSetup("setPrivateKey", [Random::generate(10)]);
	}
	
}
