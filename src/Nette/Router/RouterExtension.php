<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 2.10.16
 * Time: 22:30
 */

namespace CPTeam\Nette\Router;

use Nette\DI\CompilerExtension;

class RouterExtension extends CompilerExtension
{
	private $defaults = [];
	
	public function loadConfiguration()
	{
		$this->getConfig($this->defaults);
		
		$builder = $this->getContainerBuilder();
		
		$builder->addDefinition($this->prefix("routerFactory"))
			->setClass(RouterFactory::class);
	}
	
}
