<?php

namespace CPTeam\Nette\Router;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

/**
 * Class RouterFactory
 *
 * @package CPTeam\Core\Nette\Router
 */
class RouterFactory
{
	const BASIC_PATTERN = "<presenter>[/<action>][/<id>]";
	
	/**
	 * @var RouteList
	 */
	private $router;
	
	private $defaults = false;
	
	public function __construct($defaultRoute = true)
	{
		$this->router = new RouteList();
		$this->defaults = (bool)$defaultRoute;
	}
	
	/**
	 * @param \Nette\Application\Routers\RouteList $routeList
	 *
	 * @return \Nette\Application\Routers\RouteList
	 */
	public function addRouteList(RouteList $routeList)
	{
		$this->router[] = $routeList;
		
		return $routeList;
	}
	
	/**
	 * @param \Nette\Application\Routers\Route $route
	 */
	public function addRoute(Route $route)
	{
		$this->router[] = $route;
	}
	
	/**
	 * @param RouteList|null $routeList
	 *
	 * @return RouteList
	 */
	public function createRouter(RouteList $routeList = null)
	{
		$router = $this->getRouter();
		
		if ($routeList) {
			foreach ($routeList as $rl) {
				$router[] = $rl;
			}
		}
		
		if ($this->defaults) {
			$adminModule = new RouteList("Admin");
			$adminModule[] = new Route("//%host%/admin/" . self::BASIC_PATTERN, "Home:default");
			
			$this->addRouteList($adminModule);
			
			$this->addRoute(
				new Route(self::BASIC_PATTERN, 'Home:default')
			);
		}
		
		return $router;
	}
	
	/**
	 * Returns current router
	 *
	 * @return RouteList
	 */
	public function getRouter()
	{
		return $this->router;
	}
	
}
