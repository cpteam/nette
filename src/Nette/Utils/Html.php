<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 18.9.16
 * Time: 13:15
 */

namespace CPTeam\Nette\Utils;

/**
 * Class Html
 *
 * @package CPTeam\Nette\Utils
 */
class Html extends \Nette\Utils\Html
{
	/**
	 * @param $icon Font awesome Icon
	 */
	public function addFontAwesomeIcon($icon)
	{
		$this->addHtml(self::fontAwesomeIcon($icon));
	}
	
	public static function fontAwesomeIcon($icon)
	{
		if (substr($icon, 0, 3) !== "fa-") {
			$icon = "fa-" . $icon;
		}
		
		return self::el('i', ['class' => 'fa ' . $icon]);
	}
	
}
